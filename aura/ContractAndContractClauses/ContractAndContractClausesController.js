({
    // Calls two apex functions during initialization of the component.
    doInit: function (component, event, helper) {
        component.set("v.columns", [
            {label:"Id", fieldName:"Id", type:"Id"},
            {label:"Name", fieldName:"Name", type:"text"},
            {label:"Description", fieldName:"Description__c", type:"text"},
            {label:"Type", fieldName:"Type__c", type:"text"}
        ]);

        // Retrieves the affiliatedContractClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        var action = component.get("c.affiliatedContractClauses");
        action.setParams({ contractId: component.get("v.recordId") });
        action.setCallback(this, function (response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets two different attributes with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function (result) {
                    options.push({ value: result.Id, label: result.Name });
                });
                component.set("v.removeClauses", options);
                // Calls the clauses method of the ContractAndContractClausesHelper javascript class.   
                helper.clauses(component, event, helper, response.getReturnValue());
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);


        // Calls the addClauses method of the ContractAndContractClausesHelper javascript class.   
        helper.addClauses(component, event, helper);

    },

    // Displays the list of nonaffiliated contract clauses that can be added to the current contract.
    new: function (component, event, helper) {
        component.set("v.displayNumber", 1);
    },

    // Displays the list of affiliated contract clauses that can be removed from the current contract.
    remove: function (component, event, helper) {
        component.set("v.displayNumber", 2);
    },

    // Calls two apex functions during initialization of the component.
    addSelected: function (component, event, helper) {
        component.set("v.displayNumber", 0);

        // Retrieves the addContractClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        var action = component.get("c.addContractClauses");
        action.setParams({ contractId: component.get("v.recordId"), listContractClauseIds: component.get("v.selectedAddClauses") });
        action.setCallback(this, function (response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets two attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function (result) {
                    options.push({ value: result.Id, label: result.Name });
                });
                component.set("v.removeClauses", options);
                component.set("v.selectedAddClauses", null);
                // Calls the clauses method of the ContractAndContractClausesHelper javascript class.   
                helper.clauses(component, event, helper, response.getReturnValue());
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);

        // Calls the addClauses method of the ContractAndContractClausesHelper javascript class.   
        helper.addClauses(component);
    },

    // Calls two apex functions during initialization of the component.
    removeSelected: function (component, event, helper) {
        component.set("v.displayNumber", 0);

        // Retrieves the removeContractClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        var action = component.get("c.removeContractClauses");
        action.setParams({ contractId: component.get("v.recordId"), listContractClauseIds: component.get("v.selectedRemoveClauses") });
        action.setCallback(this, function (response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets one attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function (result) {
                    options.push({ value: result.Id, label: result.Name });
                });
                component.set("v.removeClauses", options);
                component.set("v.selectedRemoveClauses", null);
                // Calls the clauses method of the ContractAndContractClausesHelper javascript class.   
                helper.clauses(component, event, helper, response.getReturnValue());
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);

        // Calls the addClauses method of the ContractAndContractClausesHelper javascript class.   
        helper.addClauses(component);
    },

    handleChange: function (component, event, helper) {

    }
})