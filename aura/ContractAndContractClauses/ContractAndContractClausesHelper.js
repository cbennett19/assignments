({
    addClauses: function (component, event, helper) {
        // Retrieves the unaffiliatedContractClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        var action = component.get("c.unaffiliatedContractClauses");
        action.setParams({ contractId: component.get("v.recordId") });
        action.setCallback(this, function (response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets one attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function (result) {
                    options.push({ value: result.Id, label: result.Name });
                });
                component.set("v.addClauses", options);
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);
    },
    
    // Sets the contractClauses and Clauses components to the value that is passed by other functions.
    clauses: function (component, event, helper, value) {
        component.set("v.contractClauses", value);
        component.set("v.clauses", value);
    }
})