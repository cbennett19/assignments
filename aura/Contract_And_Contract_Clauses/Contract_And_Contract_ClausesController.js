({
    // Calls two apex functions during initialization of the component.
    doInit : function(component, event, helper) {
        // Retrieves the rClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        var action = component.get("c.rClauses");
        action.setParams({cId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets two different attributes with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function(result) {
                    options.push({value: result.Id, label: result.Name});
                });
                component.set("v.rClauses", options);
                component.set("v.cClauses", response.getReturnValue());
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);
        
        
        // Retrieves the aClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        action = component.get("c.aClauses");
        action.setParams({cId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets one attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function(result) {
                    options.push({value: result.Id, label: result.Name});
                });
                component.set("v.aClauses", options);
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);
    },
    
    // Displays the list of nonaffiliated contract clauses that can be added to the current contract.
    new : function(component, event, helper) {
    	component.set("v.displayNumber", 1);
	},
 
 	// Displays the list of affiliated contract clauses that can be removed from the current contract.
 	remove : function(component, event, helper) {
    	component.set("v.displayNumber", 2);
	},
        
    // Calls two apex functions during initialization of the component.
    addSelected : function(component, event, helper) {
        component.set("v.displayNumber", 0);
        
        // Retrieves the aCClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        var action = component.get("c.aCClauses");
        action.setParams({cId : component.get("v.recordId"), lCCIds : component.get("v.sAClauses")});
        action.setCallback(this, function(response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets two attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function(result) {
                    options.push({value: result.Id, label: result.Name});
                });
                component.set("v.rClauses", options);
                component.set("v.cClauses", response.getReturnValue());
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);
        
		// Retrieves the aClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        action = component.get("c.aClauses");
        action.setParams({cId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets one attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function(result) {
                    options.push({value: result.Id, label: result.Name});
                });
                component.set("v.aClauses", options);
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);        
    },
        
   	// Calls two apex functions during initialization of the component.
    removeSelected : function(component, event, helper) {
   		component.set("v.displayNumber", 0);
      	
        // Retrieves the rCClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        var action = component.get("c.rCClauses");
       	action.setParams({cId : component.get("v.recordId"), lCCIds : component.get("v.sRClauses")});
       	action.setCallback(this, function(response) {
        	// Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets one attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
            	var resultArray = response.getReturnValue();
                var options = [];
               	resultArray.forEach(function(result) {
                 	options.push({value: result.Id, label: result.Name});
              	});
                component.set("v.rClauses", options);
                component.set("v.cClauses", response.getReturnValue());
         	}
           	// If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
             	var errors = response.getError();
                if (errors) {
                  	if (errors[0] && errors[0].message) {
                    	console.log("Error message: " + errors[0].message);
                  	}
              	}
               	else {
                 	console.log("Unknown error");
               	}
         	}
    	});
        // Function Callout.
       	$A.enqueueAction(action);
            
        // Retrieves the aClauses apex function, sets the parameters of the funtion, and sets the functionality of the return from the apex function.
        action = component.get("c.aClauses");
        action.setParams({cId : component.get("v.recordId")});
        action.setCallback(this, function(response) {
            // Retrieves the status of the apex function callout.
            var state = response.getState();
            // Sets one attribute with data retrieved from the function callout if successful.
            if (state === "SUCCESS") {
                var resultArray = response.getReturnValue();
                var options = [];
                resultArray.forEach(function(result) {
                    options.push({value: result.Id, label: result.Name});
                });
                component.set("v.aClauses", options);
            }
            // If an error occured during the callout then the console logs the error.
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        // Function Callout.
        $A.enqueueAction(action);            
   	},

	handleChange : function(component, event, helper) {
                
	}
})