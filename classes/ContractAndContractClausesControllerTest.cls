@isTest
private class ContractAndContractClausesControllerTest {
    @testSetup
    private static void dataFactory () {
        // The Test account that the contract belongs to.
        Account a = new Account();
        a.Name = 'Test Account';
        
        insert a;
        
        // The test contract that is being used.
        Contract c = new Contract();
        c.AccountId = a.Id;
        c.Status = 'Draft';
        c.StartDate = date.today();
        c.ContractTerm = 10;
        
        insert c;
        
        // The first contract clause to be used in all test cases. Mostly for single positive tests.
        Contract_Clause__c contractClause = new Contract_Clause__c();
        contractClause.Name = 'Contract Clause Test';
        contractClause.Description__c = 'Description Test';
        contractClause.Type__c = 'Financial';
        
        insert contractClause;
        
        // The first contract to clause assignment to be used in all test cases.
        Contract_to_Clause_Assignment__c contractClauseAssignment = new Contract_to_Clause_Assignment__c();
        contractClauseAssignment.Name = 'Contract to Clause Assignment Test';
        contractClauseAssignment.Contract__c = c.Id;
        contractClauseAssignment.Contract_Clause__c = contractClause.Id;
        
        insert contractClauseAssignment;
        
        // The second contract clause to be used in all test cases. Mostly for negative tests.
        contractClause = new Contract_Clause__c();
        contractClause.Name = 'Contract Clause Test 2';
        contractClause.Description__c = 'Description Test 2';
        contractClause.Type__c = 'Financial';
        
        insert contractClause;        
    }
    
    @isTest
    private static void affiliatedContractClausesTest () {
        // Test Single Positive and Negative for AffiliatedContractClauses function.
        Id contractId = [SELECT Id FROM Contract].Id;
        List<Contract_Clause__c> listContractClauses = ContractAndContractClausesController.affiliatedContractClauses(contractId);
        System.assertEquals(1, listContractClauses.size());
        
        // Second contract clause to be used for test bulk.
        Contract_Clause__c contractClause = new Contract_Clause__c();
        contractClause.Name = 'Contract Clause Test 3';
        contractClause.Description__c = 'Description Test 3';
        contractClause.Type__c = 'Financial';
        
        insert contractClause;
        
        // Second contract to clause assignment to be used for test bulk.
        Contract_to_Clause_Assignment__c contractClauseAssignment = new Contract_to_Clause_Assignment__c();
        contractClauseAssignment.Name = 'Contract to Clause Assignment Test';
        contractClauseAssignment.Contract__c = contractId;
        contractClauseAssignment.Contract_Clause__c = contractClause.Id;
        
        insert contractClauseAssignment;
        
        // Recalling affiliatedContractClauses function to test for bulk data.
        listContractClauses = ContractAndContractClausesController.affiliatedContractClauses(contractId);
        System.assertEquals(2, listContractClauses.size());
    }
    
    @isTest
    private static void unaffiliatedContractClausesTest () {
        // Test single positive and negative.
        Id contractId = [SELECT Id FROM Contract].Id;
        List<Contract_Clause__c> listContractClauses = ContractAndContractClausesController.unaffiliatedContractClauses(contractId);
        System.assertEquals(1, listContractClauses.size());        
        
        // Creating second unaffiliated contract clause to be used for test bulk.
        Contract_Clause__c contractClause = new Contract_Clause__c();
        contractClause.Name = 'Contract Clause Test 3';
        contractClause.Description__c = 'Description Test 3';
        contractClause.Type__c = 'Financial';
        
        insert contractClause;
        
        // Recalling unaffiliatedContractClauses function to test for bulk data.
        listContractClauses = ContractAndContractClausesController.unaffiliatedContractClauses(contractId);
        System.assertEquals(2, listContractClauses.size());  
    }
    
    @isTest
    private static void addContractClausesTest () {
        // Test single positive and negative.
        Id contractId = [SELECT Id FROM Contract].Id;
        List<Id> listContractClauseIds = new List<Id>();
        listContractClauseIds.add([SELECT Id From Contract_Clause__c where Id not IN (SELECT Contract_Clause__c from Contract_to_Clause_Assignment__c where Contract__c =: contractId)].Id);
        List<Contract_Clause__c> listContractClauses = ContractAndContractClausesController.addContractClauses(contractId, listContractClauseIds);
        System.assertEquals(2, listContractClauses.size());               
        
        // Creating two unaffiliated contract clauses to test bulk data.
        listContractClauseIds = new List<Id>();
        List<Contract_Clause__c> listContractClauses2 = new List<Contract_Clause__c>();
        for(Integer i = 3; i < 5; i++) {
            Contract_Clause__c contractClause = new Contract_Clause__c();
            contractClause.Name = 'Contract Clause Test ' + i;
            contractClause.Description__c = 'Description Test ' + i;
            contractClause.Type__c = 'Financial';
            listContractClauses2.add(contractClause);
        }
        insert listContractClauses2; 
        
        for(Contract_Clause__c contractClause : listContractClauses2) {
            listContractClauseIds.add(contractClause.Id);
        }
        
        // Recalling addContractClauses function to test bulk data.
        listContractClauses = ContractAndContractClausesController.addContractClauses(contractId, listContractClauseIds);
        System.assertEquals(4, listContractClauses.size());

        // Recalling addContractClauses function to test negative (DMLException).
        listContractClauseIds = new List<Id>();
        listContractClauseIds.add('8003i000000gK9CAAU');
        listContractClauses = ContractAndContractClausesController.addContractClauses('8003i000000gK9CAAU', listContractClauseIds);
        System.assertEquals(null, listContractClauses);
    }
    
    @isTest
    private static void removeContractClausesTest () {
        // Test single positive and negative.
        Id contractId = [SELECT Id FROM Contract].Id;
        List<Id> listContractClauseIds = new List<Id>();
        listContractClauseIds.add([SELECT Id, Name, Description__c, Type__c FROM Contract_Clause__c WHERE id IN (SELECT Contract_Clause__c from Contract_to_Clause_Assignment__c where contract__c =: contractId)].Id);
        List<Contract_Clause__c> listContractClauses = ContractAndContractClausesController.removeContractClauses(contractId, listContractClauseIds);
        System.assertEquals(0, listContractClauses.size());
        
        // Creating two contract clauses that are affiliated with the contract to test bulk data.
        listContractClauseIds = new List<Id>();
        List<Contract_Clause__c> listContractClauses2 = new List<Contract_Clause__c>();
        for(Integer i = 3; i < 5; i++) {
            Contract_Clause__c contractClause = new Contract_Clause__c();
            contractClause.Name = 'Contract Clause Test ' + i;
            contractClause.Description__c = 'Description Test ' + i;
            contractClause.Type__c = 'Financial';
            listContractClauses2.add(contractClause);
        }
        insert listContractClauses2; 
        
        for(Contract_Clause__c contractClause : listContractClauses2) {
            listContractClauseIds.add(contractClause.Id);
        }
        
        // Creating the contract to clause assignment to affiliate the contract clauses that were just created to the contract that is being tested with.
        Integer count = 0;
        List<Contract_to_Clause_Assignment__c> listContractClauseAssignments = new List<Contract_to_Clause_Assignment__c>();
        for(Integer i = 2; i < 4; i++) {
            Contract_to_Clause_Assignment__c contractClauseAssignment = new Contract_to_Clause_Assignment__c();
            contractClauseAssignment.Name = 'Contract to Clause Assignment Test ' + i;
            contractClauseAssignment.Contract__c = contractId;
            contractClauseAssignment.Contract_Clause__c = listContractClauses2[count].Id;
            listContractClauseAssignments.add(contractClauseAssignment);
            count++;
        }
        insert listContractClauseAssignments; 
        
        // Recalling removeCClauses function to test bulk data.
        listContractClauses = ContractAndContractClausesController.removeContractClauses(contractId, listContractClauseIds);
        System.assertEquals(0, listContractClauses.size());
    }
}