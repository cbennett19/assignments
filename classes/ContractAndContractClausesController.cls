public with sharing class ContractAndContractClausesController {
    // removeClauses function fetches affiliated contract clauses of the current contract.
    @AuraEnabled
    public static List<Contract_Clause__c> affiliatedContractClauses (Id contractId) {
        List<Contract_Clause__c> listRemoveContractClauses = [SELECT Id, Name, Description__c, Type__c FROM Contract_Clause__c WHERE id IN (SELECT Contract_Clause__c from Contract_to_Clause_Assignment__c where contract__c =: contractId)];
        return listRemoveContractClauses;
    }
    
    // addClauses function fetches nonaffiliated contract clauses.
    @AuraEnabled
    public static List<Contract_Clause__c> unaffiliatedContractClauses (Id contractId) {
        List<Contract_Clause__c> listAddContractClauses = [SELECT Id, Name FROM Contract_Clause__c WHERE id not IN (SELECT Contract_Clause__c from Contract_to_Clause_Assignment__c where contract__c =: contractId)];
        return listAddContractClauses;
    }
    
    // addContractClauses function creates affiliation between available contract clauses with the current contract.
    @AuraEnabled
    public static List<Contract_Clause__c> addContractClauses (Id contractId, List<Id> listContractClauseIds) {
        List<Contract_to_Clause_Assignment__c> listContractClauseAssignments = new List<Contract_to_Clause_Assignment__c>();
        
        for (Id i : listContractClauseIds) {
            Contract_to_Clause_Assignment__c contractClause = new Contract_to_Clause_Assignment__c();
            contractClause.Contract_Clause__c = i;
            contractClause.Contract__c = contractId;
            listContractClauseAssignments.add(contractClause);
        }
        try {
            insert listContractClauseAssignments;    
        } catch (System.DmlException e) {
            return null;
        }
        

        List<Contract_Clause__c> listContractClauses = [SELECT Id, Name, Description__c, Type__c FROM Contract_Clause__c WHERE id IN (SELECT Contract_Clause__c from Contract_to_Clause_Assignment__c where contract__c =: contractId)];
        return listContractClauses;
    }
    
    // removeContractClauses removes affiliation between the current contract and certain contract clauses.
    @AuraEnabled
    public static List<Contract_Clause__c> removeContractClauses (Id contractId, List<Id> listContractClauseIds) {
        List<Contract_to_Clause_Assignment__c> listRemoveContractClauseAssignments = [SELECT id FROM Contract_to_Clause_Assignment__c WHERE Contract__c =: contractId AND Contract_Clause__c IN: listContractClauseIds];
        delete listRemoveCOntractClauseAssignments;

        List<Contract_Clause__c> listContractClauses = [SELECT Id, Name, Description__c, Type__c FROM Contract_Clause__c WHERE id IN (SELECT Contract_Clause__c from Contract_to_Clause_Assignment__c where contract__c =: contractId)];
        return listContractClauses;
    }
}